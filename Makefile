# Makefile twentyseconds cv

CC = xelatex
STORE = pdf
ARCH = archive
PDF = $(wildcard ./$(STORE)/*.pdf)

all: init resume cover clean
	@echo "Done!"

init:
	@mkdir -p ./${STORE}
	@mkdir -p ./${ARCH}

resume: ./resume.tex
	@echo "Building.... $^"
	@$(foreach var,ico.tex,$(CC) -interaction=nonstopmode -jobname=resume -output-directory=$(STORE) 'resume.tex' 1>/dev/null;)

cover: ./cover.tex
	@echo "Building.... $^"
	@$(foreach var,cover.tex,$(CC) -interaction=nonstopmode -jobname=coverletter -output-directory=$(STORE) 'cover.tex' 1>/dev/null;)

save:
	@echo "Archiving $(name) pdf"
	@mkdir -p ./${ARCH}/$(name)
	@$(foreach var,$(PDF),mv -f -u $(var) ./${ARCH}/$(name);)

clean:
	@rm -f ./${STORE}/*.aux ./${STORE}/*.dvi ./${STORE}/*.log ./${STORE}/*.out ./${STORE}/*.bak
	@echo "Cleaning.... ./${STORE}/{*.aux,*.dvi,*.log,*.out}";
