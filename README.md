# Curriculum Vitae

Based on:
* https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex
* https://github.com/posquit0/Awesome-CV

## Requirements

```
apt install texlive-xetex texlive-fonts-extra latexmk
```

## Build

```
make && make save name=company
```
